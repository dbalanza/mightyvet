$(document).ready(function() {

	$('.upper_parallax').parallax({imageSrc: 'assets/images/splash.png'});

	var myNavBar = {
	    flagAdd: true,
	    elements: [],

	    init: function (elements) {
	        this.elements = elements;
	    },

	    add : function() {
	        if(this.flagAdd) {
	            for(var i=0; i < this.elements.length; i++) {
	                document.getElementById(this.elements[i]).className += " fixed-theme";
	            }
	            this.flagAdd = false;
	        }
	    },

	    remove: function() {
	        for(var i=0; i < this.elements.length; i++) {
	            document.getElementById(this.elements[i]).className =
	                    document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
	        }
	        this.flagAdd = true;
	    }
	};
	myNavBar.init(  [
	    "header",
	    "header-container",
	    "brand"
	]);

	function offSetManager(){
	    var yOffset = 0;
	    var currYOffSet = window.pageYOffset;

	    if(yOffset < currYOffSet) {
	        myNavBar.add();
	    }
	    else if(currYOffSet == yOffset){
	        myNavBar.remove();
	    }
	}

	window.onscroll = function(e) {
	    offSetManager();
	}

	offSetManager();

  	var swiper = new Swiper('#mighty_vet_offering_swipper_container', {
	   	slidesPerView: 'auto',
		spaceBetween: 30,
		freeMode: true,
		grabCursor: true,
		navigation: {
		  nextEl: '.next_swipper_btn',
		  prevEl: '.prev_swipper_btn',
		},
		watchOverflow: true
    });


    $(".reveal_password_btn").on('click',function() {
		var pwd = $(".password_field");
		(pwd.attr('type') === 'password') ? pwd.attr('type', 'text') : pwd.attr('type', 'password');
	});

	$('.selectpicker').selectpicker({
		template: {
			caret: '<i class="fas fa-angle-down"></i>'
		}
	});


	$('body').on('click', '.next_mentor_btn', function(){
		if($(".education_team_item.active").next().length > 0){
	    	$('.education_team_item.active').addClass('rotate-left').delay(700).fadeOut(1);
			$('.education_team_item.active').next().removeClass('rotate-left rotate-right').fadeIn(400);
        	$(".education_team_item.active").removeClass("active").next("li").addClass("active");
	    }

       	if($('.education_team_item.active').is(':last-child') ){
	    	$('.next_mentor_btn').addClass('disabled');
	    }
	    else{
	    	$('.next_mentor_btn').removeClass('disabled');
	    	$('.previous_mentor_btn').removeClass('disabled');
	    }
	});

	$('body').on('click', '.previous_mentor_btn', function(){
		if($(".education_team_item.active").prev().length > 0){
	    	$('.education_team_item.active').addClass('rotate-right').delay(700).fadeOut(1);
			$('.education_team_item.active').prev().removeClass('rotate-left rotate-right').fadeIn(400);
        	$(".education_team_item.active").removeClass("active").prev("li").addClass("active");
	    }

	    if($('.education_team_item.active').is(':first-child') ){
	    	$('.previous_mentor_btn').addClass('disabled');
	    }
	    else{
	    	$('.next_mentor_btn').removeClass('disabled');
	    	$('.previous_mentor_btn').removeClass('disabled');
	    }

	}); 


	$('body').on('blur', '.sign_up_form .form_input', function(){
		check_for_if_has_value($(this));
	}); 

	$('body').on('keyup', '.sign_up_form .form_input', function(){
		check_for_if_has_value($(this))
	});

	$('body').on('click', '.learn_more_btn', function(){
		 $('html,body').animate({scrollTop: $("#what_mightyvet_section").offset().top},1000);
	});
});


function check_for_if_has_value(input_field) {
	input_field.closest('form').find('.form_input').each(function(index, el) {
		var form_input = $(this);
		if(form_input.hasClass('password_field')){
			(form_input.val() != '') ? $('.reveal_password_btn').removeClass('hidden') : $('.reveal_password_btn').addClass('hidden');
		}

		(form_input.val() != '' ) ? form_input.removeClass('no_value') : form_input.addClass('no_value');
	});

	var sign_up_button = $('#request_invite_modal .sign_up_button');
	($('.sign_up_form .form_input.no_value').length == 0) ? sign_up_button.removeClass('disabled').removeAttr('disabled') : sign_up_button.addClass('disabled').attr('disabled','disabled');
}